import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by sa on 13.02.17.
 */
public class Main {
    private static final Logger LOGGER = LogManager.getLogger(Main.class);
    public static void main(String[] args) {
        
        LOGGER.error("Hello world!");
        LOGGER.warn("Hello world!");
        LOGGER.info("Hello world!");
        LOGGER.debug("Hello world!");

    }
}
